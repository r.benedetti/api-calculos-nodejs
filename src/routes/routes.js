const express = require('express');
const controllerCalculos = require('../controllers/controllersCalculos');
const controllerDownloads = require('../controllers/controllerImagem');

const routes = express.Router();

routes.get('/mod2', controllerCalculos.mod2);
routes.get('/soma', controllerCalculos.soma);
routes.get('/sub', controllerCalculos.sub);
routes.get('/mult', controllerCalculos.mult);
routes.get('/div', controllerCalculos.div);
routes.get('/raiz', controllerCalculos.raiz);
routes.get('/potencia', controllerCalculos.potencia);

routes.get('/download', controllerDownloads.download);








module.exports = routes
