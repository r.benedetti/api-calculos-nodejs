module.exports = {
    async calculoMod2(number) {
        if(!number) return false()
        return number % 2;
    },

    async calculoSoma(number) {
        if(!number) return false()
        return eval(number)
    },

    async calculoSub(number) {
        if(!number) return false()
        return eval(number)
    },

    async calculoMul(number) {
        if(!number) return false()
        return eval(number)
    },

    async calculoDiv(number) {
        if(!number) return false()
        return eval(number)
    },

    async calculoRaiz(number) {
        if(!number) return false()
        return Math.sqrt(number)
    },

    async calculoPot(number, number2) {
        if(!number) return false()
        return Math.pow(number, number2)
        
    }
    
};