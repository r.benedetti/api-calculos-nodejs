const services = require('../services/serviceCalculos');

module.exports = {
 
   async mod2(req, res) {
        var { number } = req.body;
        if(!number) return res.status(400).end();
        const mod = await services.calculoMod2(number);
        return res.json("Success..: " + mod);

    },

    async soma(req, res) {
        var { number } = req.body;
        if(!number) return res.status(400).end();
        const som = await services.calculoSoma(number);
        return res.json("Success..: " + som);
    },

    async sub(req, res) {
        var { number } = req.body;
        if(!number) return res.status(400).end();
        const subt = await services.calculoSub(number);
        return res.json("Success..: " + subt);
    },

    async mult(req, res) {
        var { number } = req.body;
        if(!number) return res.status(400).end();
        const x = await services.calculoMul(number);
        return res.json("Success..: " + x);
    },

    async div(req, res) {
        var { number } = req.body;
        if(!number) return res.status(400).end();
        const divi = await services.calculoDiv(number);
        return res.json("Success..: " + divi);
    },

    async raiz(req, res) {
        var { number } = req.body;
        if(!number) return res.status(400).end();
        const raizz = await services.calculoRaiz(number);
        return res.json("Success..: " + raizz);
    },

    async potencia(req, res) {
        var { number, number2 } = req.body;
        if(!number, !number2) return res.status(400).end();
        const pot = await services.calculoPot(number, number2);
        return res.json("Success..: " + pot);
    } 
};

